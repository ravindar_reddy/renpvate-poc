module.exports = {
  platform: 'gitlab',
  endpoint: 'https://gitlab.com/ravindar_reddy/api/v4/',
  gitAuthor: 'ravindar <rmr.devops@gmail.com>',
  baseBranches: ['master'],
  labels: ['renovate'],

  repositories: [
    'ravindar_reddy/renpvate-poc',
  ],

  logLevel: 'info',

  requireConfig: true,
  onboarding: true,
  onboardingConfig: {
    extends: ['config:base'],
    prConcurrentLimit: 5,
  },

  enabledManagers: [
    'npm',
  ],
};
